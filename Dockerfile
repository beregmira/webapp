FROM nginxinc/nginx-unprivileged:1.18.0
COPY nginx.conf /etc/nginx/
COPY default.conf /etc/nginx/conf.d/
COPY index.html /usr/share/nginx/html/